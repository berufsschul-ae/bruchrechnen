package de.augustodwenger.fraction;

import lombok.Getter;

@Getter
public class Fraction {

    private final int numerator;
    private final int denominator;

    public Fraction(final int numerator, final int denominator) {
        if (denominator == 0) throw new DenominatorEqualsZeroException();

        this.numerator = numerator;
        this.denominator = denominator;
    }

    public Fraction div(final Fraction fraction) {
        return mult(switchingPosition(fraction));
    }

    public Fraction mult(final Fraction fraction) {
        return new Fraction(numerator * fraction.getNumerator(), denominator * fraction.getDenominator());
    }

    public Fraction add(final Fraction fraction) {
        return manipulate(fraction, +1);
    }

    public Fraction sub(final Fraction fraction) {
        return manipulate(fraction, -1);
    }

    public Fraction reduce() {
        for (int i = (int) (denominator * Math.signum(denominator)) - 1; i > 1; i--) {
            if (denominator % i == 0 && numerator % i == 0) {
                int newDenominator = denominator / i;
                int newNumerator = numerator / i;
                return new Fraction(newNumerator, newDenominator);
            }
        }
        return this;
    }

    public String mixed() {
        Fraction fraction = reduce();
        int reducedNumerator = fraction.getNumerator();
        int reducedDenominator = fraction.getDenominator();

        int integer = reducedNumerator / reducedDenominator;
        return integer + " + " + (reducedNumerator - reducedDenominator * integer) + "/" + reducedDenominator;
    }

    @Override
    public String toString() {
        return numerator + "/" + denominator;
    }

    @Override
    public boolean equals(final Object o) {
        if (o == this) {
            return true;
        } else if (o instanceof Fraction) {
            Fraction other = (Fraction) o;
            return this.reduce().getNumerator() == other.reduce().getNumerator() && this.reduce().getDenominator() == other.reduce().getDenominator();
        } else {
            return false;
        }
    }

    private Fraction switchingPosition(final Fraction fraction) {
        return new Fraction(fraction.getDenominator(), fraction.getNumerator());
    }

    private Fraction manipulate(final Fraction fraction, final int manipulator) {
        CommonDenominator commonDenominator = new CommonDenominator(this, fraction);
        Fraction fraction1 = commonDenominator.getFraction1();
        int numerator = fraction1.getNumerator() + manipulator * commonDenominator.getFraction2().getNumerator();
        return new Fraction(numerator, fraction1.getDenominator());
    }

}
