package de.augustodwenger.fraction;

import java.util.ArrayList;
import java.util.List;

public class CommonDenominator {
    private final Fraction fraction1;
    private final Fraction fraction2;
    private final List<Fraction> commonDenominatedCache = new ArrayList<>(2);

    public CommonDenominator(final Fraction fraction1, final Fraction fraction2) {
        this.fraction1 = fraction1;
        this.fraction2 = fraction2;
    }

    public Fraction getFraction1() {
        if (!isCached()) {
            calculateCommonDenominator();
        }
        return commonDenominatedCache.get(0);
    }

    public Fraction getFraction2() {
        if (!isCached()) {
            calculateCommonDenominator();
        }
        return commonDenominatedCache.get(1);
    }

    private boolean isCached() {
        return commonDenominatedCache.size() == 2;
    }

    private void calculateCommonDenominator() {
        commonDenominatedCache.add(fraction1.mult(new Fraction(fraction2.getDenominator(), fraction2.getDenominator())));
        commonDenominatedCache.add(fraction2.mult(new Fraction(fraction1.getDenominator(), fraction1.getDenominator())));
    }
}
