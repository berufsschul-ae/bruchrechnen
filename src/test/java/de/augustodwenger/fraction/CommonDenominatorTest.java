package de.augustodwenger.fraction;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CommonDenominatorTest {

    Fraction fraction2;
    Fraction fraction1;
    CommonDenominator commonDenominator;


    @Before
    public void setUp() {
        fraction1 = new Fraction(4,2);
        fraction2 = new Fraction(2,4);
        commonDenominator = new CommonDenominator(fraction1, fraction2);
    }

    @Test
    public void shouldSetToCommonDenominatorForFraction1() {

        assertEquals(16, commonDenominator.getFraction1().getNumerator());
        assertEquals(8, commonDenominator.getFraction1().getDenominator());
    }

    @Test
    public void shouldSetToCommonDenominatorForFraction2() {

        assertEquals(4, commonDenominator.getFraction2().getNumerator());
        assertEquals(8, commonDenominator.getFraction2().getDenominator());
    }
}