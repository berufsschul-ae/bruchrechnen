package de.augustodwenger.fraction;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class FractionTest {

    private Fraction fraction1;
    private Fraction fraction2;

    @Before
    public void before() {
        fraction1 = new Fraction(2, 1);
        fraction2 = new Fraction(2, 1);
    }

    @Test(expected = DenominatorEqualsZeroException.class)
    public void constructorException() {
        new Fraction(1, 0);
        fail();
    }

    @Test
    public void toStringTest() {
        assertEquals("2/1", fraction1.toString());
    }

    @Test
    public void divMain() {
        Fraction result = fraction1.div(fraction2);

        assertEquals("2/2", result.toString());
    }

    @Test
    public void divWithNegativeNum() {
        Fraction fraction1 = new Fraction(-2, 1);
        Fraction fraction2 = new Fraction(2, -1);
        Fraction result = fraction1.div(fraction2);

        assertEquals("2/2", result.toString());
    }

    @Test
    public void divWithNegativeNum2() {
        Fraction fraction1 = new Fraction(-2, -1);
        Fraction result = fraction1.div(fraction2);

        assertEquals("-2/-2", result.toString());
    }


    @Test
    public void multMain() {
        Fraction result = fraction1.mult(fraction2);
        assertEquals("4/1", result.toString());
    }

    @Test
    public void multWithNegativeNum() {
        Fraction fraction1 = new Fraction(-2, -1);
        Fraction result = fraction1.mult(fraction2);

        assertEquals("-4/-1", result.toString());
    }

    @Test
    public void addMain() {
        Fraction fraction1 = new Fraction(2, 4);
        Fraction fraction2 = new Fraction(2, 4);
        Fraction fraction3 = fraction1.add(fraction2);

        assertEquals("16/16", fraction3.toString());
    }

    @Test
    public void addNegativeDenominator() {
        Fraction fraction1 = new Fraction(2, -4);
        Fraction fraction2 = new Fraction(2, 4);
        Fraction fraction3 = fraction1.add(fraction2);

        assertEquals("0/-16", fraction3.toString());
    }

    @Test
    public void subMain() {
        Fraction fraction1 = new Fraction(4, 4);
        Fraction fraction2 = new Fraction(2, 4);
        Fraction fraction3 = fraction1.sub(fraction2);

        assertEquals("8/16", fraction3.toString());
    }

    @Test
    public void subWithNegativeDenominator() {
        Fraction fraction1 = new Fraction(4, -4);
        Fraction fraction2 = new Fraction(2, 4);
        Fraction fraction3 = fraction1.sub(fraction2);

        assertEquals("24/-16", fraction3.toString());
    }

    @Test
    public void reduceMain() {
        Fraction fraction = new Fraction(4, 8);
        Fraction reducedFraction = fraction.reduce();

        assertEquals("1/2", reducedFraction.toString());
    }

    @Test
    public void reduceWithNegativeDenominator() {
        Fraction fraction = new Fraction(4, -8);
        Fraction reducedFraction = fraction.reduce();

        assertEquals("1/-2", reducedFraction.toString());
    }

    @Test
    public void reduceNegative() {
        Fraction fraction = new Fraction(-4, -8);
        Fraction reducedFraction = fraction.reduce();

        assertEquals("-1/-2", reducedFraction.toString());
    }

    @Test
    public void reduceMitNullNumerator() {
        Fraction fraction = new Fraction(0, 4);
        Fraction reducedFraction = fraction.reduce();

        assertEquals("0/2", reducedFraction.toString());
    }

    @Test
    public void mixedFractionMain() {
        Fraction fraction = new Fraction(24, 12);

        assertEquals("2 + 0/2", fraction.mixed());
    }

    @Test(expected = DenominatorEqualsZeroException.class)
    public void mixedFractionWithDenominatorNull() {
        Fraction fraction = new Fraction(24, 0);

        fraction.mixed();
        fail();
    }

    @Test
    public void mixedFractionWithDenominatorNegative() {
        Fraction fraction = new Fraction(3, -2);

        assertEquals("-1 + 1/-2", fraction.mixed());
    }

    @Test
    public void mixedFractionWithNegativeNum() {
        Fraction fraction = new Fraction(-3, -2);

        assertEquals("1 + -1/-2", fraction.mixed());
    }

    @Test
    public void shouldReturnEqualsMethod() {
        assertTrue("Both objects are equal", fraction1.equals(fraction2));
    }

    @Test
    public void shouldBeNotEqual() {
        assertFalse(fraction1.equals(new Object()));
    }

    @Test
    public void shouldNotBeEqual() {
        fraction2 = new Fraction(4,2);
        assertFalse("Both objects are equal", fraction1.equals(fraction2));
    }

    @Test
    public void shouldNotHaveTheSameHash() {
        fraction2 = new Fraction(4,2);
        assertFalse("Both objects should have different hash codes", fraction1.hashCode() == fraction2.hashCode());
    }

}
