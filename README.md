# Bruchrechnen

*This package brings fractions as objects to java.*

[![pipeline status](https://gitlab.com/berufsschul-ae/bruchrechnen/badges/master/pipeline.svg)](https://gitlab.com/berufsschul-ae/bruchrechnen/commits/master)
[![coverage report](https://gitlab.com/berufsschul-ae/bruchrechnen/badges/master/coverage.svg)](https://gitlab.com/berufsschul-ae/bruchrechnen/commits/master)

This was the first programming task from the vocational school Itech.
The task was to get started with the basics from java. For this we should create a representation of a fraction in java and serve some methods to manipulate them.
